Projet r�alis� en collaboration avec Cl�ment Oursin.

Le jeu se veut �tre une sorte de runner (bien que l'on ai un vaisseau dans l'espace) sans fin, avec difficult� croissante,
le but �tant de cr�er un jeu difficile mais � l'interface simple, "easy to learn, hard to master" o� l'on souhaite �clater
son pr�c�dent score.

Probl�mes du projet (� corriger) :

- Stockage du score

- Ajouter la barre du hud au canvas mais probl�me : Les particules (fum�e repr�sentant la vie)
ne peuvent pas s'ajouter au canvas et se trouveront donc au second plan.

- Probl�me avec le Time.time qui �tait reset au relancement de l'appli en pc mac & linux standalone mais
qui n'est pas reset sur mobile, on a donc une vitesse des projectiles qui ne se reset pas en recommen�ant.

- Ajout d'un menu (start, highscore, d�sactiver le son)

- Etablir une taille des �l�ments du hud fixe : Le joystick est parfait sur de petites devices a faible r�solutions
mais plus la r�solution est �lev�e, moins il est pratique.

- Am�liorer l'algorythme de spawn et d'acc�l�ration (effets �tranges des ast�roides si le temps passe beaucoup + quasi invincibilit�
si le joueur se place au milieu de l'�cran en x et tout en haut en y)