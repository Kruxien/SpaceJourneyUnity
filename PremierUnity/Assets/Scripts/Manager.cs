﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {

	public GameObject[] respawns;
	private Vector3 rightTopCameraBorder;
	private Vector3 leftTopCameraBorder;
	private Vector3 rightBottomCameraBorder;
	private Vector3 leftBottomCameraBorder;
	private Vector3 siz;

	public float score = 0;
	public Text scoreTxt;


	//sachant que le temps ne se reset pas, startTime prend la valeur du temps au relancement
	//et permet de calculer GameTime, qui est le temps adapté à la partie en cours.
	public float GameTime;
	public float startTime;

	public GameObject quad;

	public GameObject label;
	public GameObject buttonRetry;
	public GameObject buttonQuit;

	private Button retry;
	private Button quit;

	private bool gameOver;
	private bool restart;


	// Use this for initialization
	void Start () {

		gameOver = false;
		restart = false;

		startTime = (float)(Time.time);

		GameObject label = GameObject.FindWithTag ("labelOver");
		GameObject buttonRetry = GameObject.FindWithTag ("buttonRetry");
		GameObject buttonQuit = GameObject.FindWithTag ("buttonQuit");

		Button retry = buttonRetry.GetComponent<Button> ();
		Button quit = buttonQuit.GetComponent<Button> ();

		retry.onClick.AddListener (RetryOnClick);
		quit.onClick.AddListener (QuitOnClick);

		label.SetActive (false);
		buttonRetry.SetActive (false);
		buttonQuit.SetActive (false);

		GameObject.FindGameObjectWithTag ("life1").GetComponent<ParticleSystem>().enableEmission = false;
		GameObject.FindGameObjectWithTag ("life2").GetComponent<ParticleSystem>().enableEmission = false;

		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, 0));
		leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, 0));
		rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 1, 0));
	}

	// Update is called once per frame
	void Update () {
		//du moment que le jeu n'est pas 'game over' on execute tout
		if (!gameOver) {
			//en enlevant le temps du lancement au temps courant, on a le temps de la partie en cours
			GameTime = (float)(Time.time) - startTime;

			respawns = GameObject.FindGameObjectsWithTag ("asteroid");
			if (GameTime > 5) {
				if (respawns.Length > 0) {
					siz.x = respawns [0].GetComponent<SpriteRenderer> ().bounds.size.x * gameObject.transform.localScale.x;
					siz.y = respawns [0].GetComponent<SpriteRenderer> ().bounds.size.y * gameObject.transform.localScale.y;
				}
					

				if (respawns.Length < 10) {
					if (Random.Range (1, 100) == 50 || respawns.Length < 8) {
						Vector3 tmpPos = new Vector3 (rightBottomCameraBorder.x + (siz.x / 2),
							                 Random.Range (rightBottomCameraBorder.y + (siz.y / 2), (rightTopCameraBorder.y - (siz.y / 2))),
							                 transform.position.z);
						GameObject gy = Instantiate (Resources.Load ("asteroidSP"), tmpPos, Quaternion.identity) as GameObject;
					}
				}
			}
			//une façon simple pour que le score augmente de façon exponentielle avec le temps
			score = (int)(GameTime * GameTime);
			scoreTxt.text = "" + score;

			//si le ship est détruit, il n'existe plus, le jeu est donc terminé
			if (GameObject.FindGameObjectWithTag ("ship") == null) {
				gameOver = true;
			}
		}
		//on arrive directement ici si gameover est Vrai
		else {
			//J'affiche tout le menu de gameover, le texte et les deux boutons
			label.SetActive (true);
			buttonRetry.SetActive (true);
			buttonQuit.SetActive (true);
		}
		
	}

	//bouton Retry
	void RetryOnClick(){
		//Je relance comme ça pour que les composants soient rechargés,
		//malheuresement ça n'a pas corrigé le problème du temps. (à améliorer)
		int scene = SceneManager.GetActiveScene ().buildIndex;
		SceneManager.LoadScene (scene, LoadSceneMode.Single);
	}

	//bouton quitter
	void QuitOnClick(){
		Application.Quit();
	}
}
