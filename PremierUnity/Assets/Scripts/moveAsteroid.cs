﻿using UnityEngine;
using System.Collections;

public class moveAsteroid : MonoBehaviour {

	private float speed;

	private float step_x;
	private float step_y;
	private float rapport;

	private Vector2 movement;

	//son de mort, c'est une enclume dans minecraft et je trouve qu'il met un stop.
	public AudioClip deathSound;

	private Vector3 positionStart;
	private Vector3 positionEnd;

	private Vector3 rightTopCameraBorder;
	private Vector3 leftTopCameraBorder;
	private Vector3 rightBottomCameraBorder;
	private Vector3 leftBottomCameraBorder;
	private Vector3 siz;

	private bool triggered1;
	private bool triggered2;

	public float GameTime;
	public float startTime;

	// Use this for initialization
	void Start () {

		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, 0));
		leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, 0));
		rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 1, 0));

		siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

		//je trouve une position de départ à mon asteroide (un y aléatoire sur la droite de l'écran de jeu)
		Vector3 positionStart = new Vector3 (rightBottomCameraBorder.x + (siz.x / 2), Random.Range (rightBottomCameraBorder.y + (siz.y / 2), rightTopCameraBorder.y - siz.y), 0);
		transform.position = positionStart;

		//je trouve une position d'arrivée à mon asteroide (un y aléatoire sur la gauche de l'écran de jeu)
		Vector3 positionEnd = new Vector3 (leftBottomCameraBorder.x + siz.x, Random.Range (leftBottomCameraBorder.y, leftTopCameraBorder.y-siz.y- 1), 0);

		//calcul de la taille du vecteur Départ -> arrivée
		step_x = (positionEnd.x - positionStart.x);
		step_y = (positionEnd.y - positionStart.y);

		//rapport entre les deux qui va être utilisé pour la vitesse en fonction de la taille de l'écran
		rapport = step_x / step_y;

		Debug.Log (step_x);
		Debug.Log (step_y);

	}

	void	OnTriggerEnter2D(Collider2D	collider)	{	
		if(collider.name	==	"myship")	
		{	
			//premier choc --> fumée blanche qui sort du moteur
			if (GameObject.FindGameObjectWithTag ("life1").GetComponent<ParticleSystem>().enableEmission == false && GameObject.FindGameObjectWithTag ("life2").GetComponent<ParticleSystem>().enableEmission == false) {
				GetComponent<AudioSource>().Play ();
				GameObject.FindGameObjectWithTag ("life1").GetComponent<ParticleSystem> ().enableEmission = true;

			//deuxieme choc --> fumée noire qui sort du moteur
			} else if (GameObject.FindGameObjectWithTag ("life2").GetComponent<ParticleSystem>().enableEmission == false) {
				GetComponent<AudioSource>().Play ();
				GameObject.FindGameObjectWithTag ("life1").GetComponent<ParticleSystem> ().enableEmission = false;
				GameObject.FindGameObjectWithTag ("life2").GetComponent<ParticleSystem> ().enableEmission = true;
			//troisième choc --> mort
			} else {
				GetComponent<AudioSource>().clip = deathSound;
				GetComponent<AudioSource>().Play();
				Destroy (GameObject.FindGameObjectWithTag ("ship"));	
			}
		}	
	}

	// Update is called once per frame
	void Update () {

		siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

		//on détruit l'astéroide a la sortie de l'écran
		if (transform.position.x < leftTopCameraBorder.x - (siz.x / 2))
			Destroy(gameObject);


		//Screen.width/100 me permet de fixer une vitesse en fonction de la taille de l'écran,
		//le '/siz.x' me permet que cette valeure soit en pixels par secondes.
		//le '*Time.time/50' permet une augmentation de la vitesse avec le temps
		//la dernière soustraction permet à la vitesse d'avoir une bonne vitesse de départ, 
		//qui devient quasi inéxistante une fois que le temps entre en jeu
		speed = -(((Screen.width / 100) / siz.x) * (Time.time / 50)) - (Screen.width / 100) / siz.x;
		

		//on set le mouvement
		movement = new Vector2 (speed, speed/rapport);
		GetComponent<Rigidbody2D>().velocity = movement;
	}
}