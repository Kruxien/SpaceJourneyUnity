﻿using UnityEngine;
using System.Collections;

public class scrollBackground : MonoBehaviour {

	public float speed = 1f;
	private Vector2 movement;



	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		
		movement = new Vector2 ( (Time.time)+speed, 0);

		GetComponent<Renderer>().material.mainTextureOffset = movement;
	}
}
