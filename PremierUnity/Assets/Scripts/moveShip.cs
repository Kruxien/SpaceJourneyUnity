﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;


public class moveShip : MonoBehaviour {

	public float speed;
	private Vector2 movement;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//on récupère les données du Joystick pour déplacer le joueur
		movement = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical")) * speed;
		GetComponent<Rigidbody2D> ().velocity = movement;
	}

	void FixedUpdate(){

	}
}


	