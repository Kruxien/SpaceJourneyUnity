﻿using UnityEngine;
using System.Collections;

//réalisé en cours
public class posShip : MonoBehaviour {

	private Vector3 rightTopCameraBorder;
	private Vector3 leftTopCameraBorder;
	private Vector3 rightBottomCameraBorder;
	private Vector3 leftBottomCameraBorder;
	private Vector3 siz;

	void Start () {
		leftBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, 0));
		rightBottomCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, 0));
		leftTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 1, 0));
		rightTopCameraBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 1, 0));

		gameObject.transform.position = new Vector3 (leftTopCameraBorder.x + (siz.x / 2),transform.position.y , 0);
	}



	void Update () {
		siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
		siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

		if (transform.position.y < leftBottomCameraBorder.y + (siz.y / 2))
			gameObject.transform.position = new Vector3 (transform.position.x,
				leftBottomCameraBorder.y + (siz.y / 2),
				transform.position.z);

		if (transform.position.y > leftTopCameraBorder.y - (siz.y) - 1)
			gameObject.transform.position = new Vector3 (transform.position.x,
				leftTopCameraBorder.y - (siz.y)- 1,
				transform.position.z);

		if (transform.position.x > rightTopCameraBorder.x - (siz.x / 2))
			gameObject.transform.position = new Vector3 (rightTopCameraBorder.x - (siz.x / 2),
				transform.position.y,
				transform.position.z);

		if (transform.position.x < leftTopCameraBorder.x + (siz.x / 2))
			gameObject.transform.position = new Vector3 (leftTopCameraBorder.x + (siz.x / 2),
				transform.position.y,
				transform.position.z);
		

	}
}
